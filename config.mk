# Translations
PRODUCT_PACKAGES += \
    ApertureTranslation \
    KernelSUTranslation  \
    ParanoidSenseTranslation

PRODUCT_PACKAGES += \
    POSFrameworksTranslation \
    POSSettingsTranslation \
    POSSystemUITranslation
